
# Curve Group #

* The curve group endpoint enables you to create curve groups.

* These endpoints use the http POST method and the details are provided in a json formatted parameter.

* Curve groups allow permissions for reading and writing data and metadata to be assigned to users at a group level.

* The curve group can be created using a filter or by specifying details for each individual curve in the group.

* The userGroupName specified should be an existing userGroup which you have permissions for, if not supplied this will default to your userGroupName.



* Prior to running these samples edit the config file to contain your server name and your apikey, and edit the line of code starting 'configfile = ' to be the fullpathname of your config file.




## Creating a curve group using individual curveIds ##

* Set useFilter to false in the Json parameter.

* The groupName specified should be a unique curve group name for the curve group you are creating.

* For each individual curveID in permissionsForCurves set readMeta, writeMeta, readData and writeData to either true or false.




## Creating a curve group using a filter ##

* Set useFilter to true in the Json parameter.

* Specify the search filter which will be used to create the curveGroup.

* The groupName specified should be a unique curve group name for the curve group you are creating.

* Set the readMeta, writeMeta, readData and writeData permissions for all the curves in the group using FilterReadMeta, FilterWriteMeta, FilterReadData and FilterWriteData.




## Editing a curve group specifying individual curveIds ##

* The curveGroupId must be an existing curveGroupId.

* Set useFilter to false in the Json parameter, this must be the same as the existing curveGroup.

* The groupName specified should be the original curveGroupName or a unique curve group name for the curve group you are editing.

* For each individual curveID in permissionsForCurves set readMeta, writeMeta, readData and writeData to either true or false.




## Editing a curve group using a filter ##

* The curveGroupId must be an existing curveGroupId.

* Set useFilter to true in the Json parameter, this must be the same as the existing curveGroup.

* Specify the search filter which will be used to edit the curveGroup.

* The groupName specified should be the original curveGroupName or a unique curve group name for the curve group you are editing.

* Set the readMeta, writeMeta, readData and writeData permissions for all the curves in the group using FilterReadMeta, FilterWriteMeta, FilterReadData and FilterWriteData.




* For further information on search filters please refer to the REST API or Web Application user guides.

